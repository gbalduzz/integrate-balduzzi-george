#include"integrate.h"
double Integrate(double (*f)(double), double x_min, double x_max, int n_bin){
  double dx=(x_max-x_min)/n_bin/2;
  double sum=(f(x_min)+f(x_max))/2;
  for(int i=0; i<n_bin;i++)  sum+=f(x_min+(2*i+1)*dx)*2+f(x_min+2*i*dx);
 return sum*dx*2/3;
}
